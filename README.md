# OMTP Project


## Overview
This repository is gathering of all the exercises in the course object manipulation and task planning, by Group 865 of the second semester Msc Robotics at Aalborg university.

###### (Make sure to clone the relevant branch! The master branch contains all the exercises, while the ROS_MELODIC branch contains the solutions to them. The master branch has the exercises without the catkin workspace (src))

## Table of contents
* [Prerequisites](#prerequisites)
* [Assignments](#assignment)
* [Usage](#usage)
* [Contributing](#contributing)
* [Authors](#authors)

## Prerequisites

`Ubuntu 18.04` and `ROS Melodic` is required.


## Assignments
#### Lecture 1 Assignment:
URDF and Xacro manipulation, in the form of moving stuff around, and changing parameters. The assignment was based on a given environment, which was according to the assignment changed. Two robot arms were added (UR5 and AAB IRB 6640)


#### Lecture 3 and 4 Assignments (not mandatory):  
Implement and test DMPs. Using MATLAB and Mujoco (Haptix). From exercises 1 to 4. Motion, pick and place, contacts on simulated forces


#### Lecture 5 Assignment:
Learn the Moveit package by configuring the two UR5 robots using the MoveIt Setup Assistant and create some simple movements. A script is then made with the movements of the robots, which are executet using the MoveIt Commander.
The third assignment is the completion of the script to do a pick and place movement
![UR robot Moving](/Figures/Assignment3.gif)

#### Lecture 6 Assignment:
Set up and configure two logical cameras in the world and verify with the tf three that they are present.
![Figure of the tf three](/Figures/frames.png).
![Figure of the the workspace](/Figures/asgn2.png).
Implement an object and complete the lecture6_assignement3 script, which will then transform the pose of the object to the suction_cup for robot2.

In the fourth assignment a python script for the second robot has been made that pick up an object next to the robot and place it in to the bin. Our solution can be seen in the gif.
![UR robot Moving](/Figures/lecture6.gif)

#### Lecture 7 Assignment:
Using FlexBE to make a state machine design that use the conveyor to move objects into the logical camera for the robot1 to pick and place.  The solution can be seen in the gif 
![UR robot Moving](/Figures/lecture7.gif)


## Usage

Clone the repository, and build it. The environment can be seen by running:
```bash
$ roslaunch omtp_support visualize_omtp_factory.launch
``` 



## Contributing
All the work will be pushed by the project group.

## Authors

Ditte Damgaard Albertsen:	        dalber16@student.aau.dk

Rahul Ravichandran:			rravic19@student.aau.dk

Rasmus Thomsen:				rthoms16@student.aau.dk

Mathiebhan Mahendran:			mmahen15@student.aau.dk

Alexander Staal:			astaal16@student.aau.dk

Carolina Bucle Infinito:                cgomez19@student.aau.dk
